package net.revature.leslie.stroh.service;

import java.util.ArrayList;
import java.util.List;

import net.revature.leslie.stroh.dao.Employee;
import net.revature.leslie.stroh.dao.ReimbursementRequest;
import net.revature.leslie.stroh.db.EmployeeDao;
import net.revature.leslie.stroh.db.EmployeeDaoImpl;
import net.revature.leslie.stroh.util.InputVerification;

public class EmployeeService {
	EmployeeDao employeeDao;
	public EmployeeService() {
		this.employeeDao = new EmployeeDaoImpl();
	}
	public List<ReimbursementRequest> getPendingReimbursementRequests(String id){
		if(!InputVerification.isLong(id)) return null;
		return employeeDao.getPendingReimbursementRequests(new Employee(Long.valueOf(id)));
	}
	
	public List<ReimbursementRequest> getResolvedReimbursementRequests(String id){
		if(!InputVerification.isLong(id)) return null;
		return employeeDao.getResolvedReimbursementRequests(new Employee(Long.valueOf(id)));
	}
	
	public boolean submitReimbursementRequest(ReimbursementRequest rr,String id) {
		if(!InputVerification.isLong(id) || 
		   !employeeDao.checkIfEmployeeExists(new Employee(Long.valueOf(id)))||
		   Long.valueOf(id) != rr.getRequestorReference()) {
			return false;
		}
		return employeeDao.submitReimbursementRequest(rr);
	}
}
