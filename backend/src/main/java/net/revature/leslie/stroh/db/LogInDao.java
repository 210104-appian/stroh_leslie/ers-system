package net.revature.leslie.stroh.db;

import net.revature.leslie.stroh.dao.Employee;

public interface LogInDao {

	Employee getEmployeeFromLoginCredentials(Employee employee);
}
