package net.revature.leslie.stroh.service;

import java.util.ArrayList;

import net.revature.leslie.stroh.dao.Employee;
import net.revature.leslie.stroh.dao.EmployeeProfile;
import net.revature.leslie.stroh.dao.ReimbursementRequest;
import net.revature.leslie.stroh.db.EmployeeDao;
import net.revature.leslie.stroh.db.EmployeeDaoImpl;
import net.revature.leslie.stroh.util.InputVerification;

public class EmployeeProfileService {
    EmployeeDao employeeDao = new EmployeeDaoImpl();
	
	public EmployeeProfile getProfile(String id) {
		if(!InputVerification.isLong(id)) return null;
		return employeeDao.getEmployeeProfile(new Employee(Long.valueOf(id)));
	}
}
