package net.revature.leslie.stroh.servlets;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.fasterxml.jackson.databind.ObjectMapper;

import net.revature.leslie.stroh.dao.Employee;
import net.revature.leslie.stroh.service.LogInService;


@SuppressWarnings("serial")
public class LogInServlet extends HttpServlet {
	LogInService loginService = new LogInService();
	private static Logger log = Logger.getRootLogger();
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		BufferedReader br = request.getReader();
		String json = "";
		for(String line = br.readLine(); line != null; line = br.readLine()) {
			json += line;
		}
		ObjectMapper om = new ObjectMapper();
		Employee employee = om.readValue(json, Employee.class);
		
		//The idea is to take care of data validation on the client side and only receive correct requests from the user
		Employee requestedEmployee = loginService.logIn(employee);
		if(requestedEmployee == null) {
			log.info("unsuccessful login attempt. input: " + json);
			response.setStatus(401);
			return;
		}
		String jsonItems = om.writeValueAsString(requestedEmployee);
		PrintWriter pw = response.getWriter();
		pw.write(jsonItems);
		pw.close();
	}
}
