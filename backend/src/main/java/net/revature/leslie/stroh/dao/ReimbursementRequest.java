package net.revature.leslie.stroh.dao;

public class ReimbursementRequest {
	public enum Status{
		PENDING,REJECTED,APPROVED
	}
	
	long id;
	Status status;
	double amt;
	String description;
	long approvalReference;
	long requestorReference;
	
	
	public ReimbursementRequest() {
		//DEFAULT
	}


	public long getId() {
		return id;
	}


	public void setId(long id) {
		this.id = id;
	}


	public Status getStatus() {
		return status;
	}


	public void setStatus(Status status) {
		this.status = status;
	}


	public double getAmt() {
		return amt;
	}


	public void setAmt(double amt) {
		this.amt = amt;
	}


	public long getApprovalReference() {
		return approvalReference;
	}


	public void setApprovalReference(long approvalReference) {
		this.approvalReference = approvalReference;
	}


	public long getRequestorReference() {
		return requestorReference;
	}


	public void setRequestorReference(long requestorReference) {
		this.requestorReference = requestorReference;
	}


	public String getDescription() {
		return description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	@Override
	public String toString() {
		return "ReimbursementRequest [id=" + id + ", status=" + status + ", amt=" + amt + ", description=" + description
				+ ", approvalReference=" + approvalReference + ", requestorReference=" + requestorReference + "]";
	}
	
	
	
	
	
	
	
}
