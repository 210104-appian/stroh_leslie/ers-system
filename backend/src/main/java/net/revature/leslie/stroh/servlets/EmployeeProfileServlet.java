package net.revature.leslie.stroh.servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.fasterxml.jackson.databind.ObjectMapper;

import jdk.internal.org.jline.utils.Log;
import net.revature.leslie.stroh.dao.EmployeeProfile;
import net.revature.leslie.stroh.service.EmployeeProfileService;

/*         GET /employee/profile?={employee-id}          */

@SuppressWarnings("serial")
public class EmployeeProfileServlet extends HttpServlet{
	EmployeeProfileService profileService = new EmployeeProfileService();
	private static Logger log = Logger.getRootLogger();
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		String id = request.getParameter("id");
		if(id == null) {
			log.info("A request was made to GET /employee/profile without proper authorization parameter");
			response.setStatus(412);
			return;
		
		}
		EmployeeProfile profile =profileService.getProfile(id);
		if(profile == null) {
			response.setStatus(412);
			return;
		}
		
		ObjectMapper om = new ObjectMapper();
		String jsonItems = om.writeValueAsString(profile);
		PrintWriter pw = response.getWriter();
		pw.write(jsonItems);
		pw.close();
	}
}
