package net.revature.leslie.stroh.db;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.log4j.Logger;

import net.revature.leslie.stroh.dao.Employee;
import net.revature.leslie.stroh.dao.EmployeeProfile;
import net.revature.leslie.stroh.dao.ReimbursementRequest;

public class ManagerDaoImpl implements ManagerDao {
	private static Logger log = Logger.getRootLogger();
	
	@Override
	public List<Employee> getAllPendingReimbursements() {
		String sql = "SELECT * FROM EMPLOYEE e JOIN REIMBURSEMENT r ON e.EMPLOYEE_ID = r.REQUESTER WHERE STATUS = 'PENDING'";
	   return doEmployeeReimbursementJoinQuery(sql);
	}

	@Override
	public List<Employee> getAllResolvedReimbursements() {
		String sql = "SELECT * FROM EMPLOYEE e JOIN REIMBURSEMENT r ON e.EMPLOYEE_ID = r.REQUESTER WHERE STATUS != 'PENDING'";
	   return doEmployeeReimbursementJoinQuery(sql);
	}
	
	private List<Employee> doEmployeeReimbursementJoinQuery(String sql){
		try(Connection connection = ConnectionUtil.getConnection();
				Statement stmt = connection.createStatement()){
				ResultSet employeePendingRequestJoin = stmt.executeQuery(sql);
				HashMap<Long,Employee> employees = new HashMap<Long,Employee>();
				List<ReimbursementRequest> requests = new ArrayList<ReimbursementRequest>();
				fillEmployeesAndRequestsFromResultSet(employees,requests,employeePendingRequestJoin);
				for(ReimbursementRequest request : requests) {
					employees.get(request.getRequestorReference()).getRequests().add(request);
				}
				return new ArrayList<Employee>(employees.values());
			}catch(SQLException e) {
				e.printStackTrace();
				log.error("SQL Exception at ManagerDao.doEmployeeReimbursement");
				return null;
			}
	}
	
	private void fillEmployeesAndRequestsFromResultSet(HashMap<Long,Employee> employees, List<ReimbursementRequest> requests, ResultSet rs) throws SQLException {
		for(boolean isValid = rs.next(); isValid; isValid = rs.next()) {
			Employee employee = new Employee();
			employee.setId(rs.getLong("EMPLOYEE_ID"));
			employee.setUsername(rs.getString("USERNAME"));
			if(!employees.containsKey(employee.getId())) {
				employees.put(employee.getId(), employee);
			}
			ReimbursementRequest rr = new ReimbursementRequest();
			rr.setRequestorReference(rs.getLong("REQUESTER"));
			rr.setId(rs.getLong("REIMBURSEMENT_ID"));
			rr.setStatus(ReimbursementRequest.Status.valueOf(rs.getString("STATUS")));
			rr.setAmt(rs.getDouble("AMT"));
			rr.setDescription(rs.getString("DESCRIPTION"));
			rr.setApprovalReference(rs.getLong("APPROVER"));
			requests.add(rr);
		}
	}
	
	
	

	@Override
	public boolean checkIfManager(Employee employee) {
		if(employee == null) return false;
		String sql = "SELECT IS_MANAGER FROM EMPLOYEE WHERE EMPLOYEE_ID = ?";
		try(Connection connection = ConnectionUtil.getConnection();
			PreparedStatement pstmt = connection.prepareStatement(sql)){
			pstmt.setLong(1, employee.getId());
			ResultSet rs = pstmt.executeQuery();
			if(!rs.next()) return false;
			return rs.getString(1).equals("Y");
			
		}catch(SQLException e) {
			e.printStackTrace();
			log.error("SQL Exception at ManagerDao.checkIfManager argument:" + employee);
			return false;
		}
	}
	
	@Override
	/*Returns an employee with their profile*/
	public List<Employee> getAllEmployees() {
		String sql = "SELECT * FROM EMPLOYEE e JOIN PROFILE p ON e.EMPLOYEE_ID = p.PROFILE_OWNER";
		try(Connection connection = ConnectionUtil.getConnection();
				Statement stmt = connection.createStatement()){
				ResultSet employeesWithProfileJoin = stmt.executeQuery(sql);
				List<Employee> result = new ArrayList<Employee>();
				for(boolean isValid = employeesWithProfileJoin.next(); isValid; isValid = employeesWithProfileJoin.next()) {
					Employee employee = new Employee();
					EmployeeProfile profile = new EmployeeProfile();
					profile.setFirstName(employeesWithProfileJoin.getString("FIRST_NAME"));
					profile.setLastName(employeesWithProfileJoin.getString("LAST_NAME"));
					profile.setTitle(employeesWithProfileJoin.getString("TITLE"));
					
					employee.setId(employeesWithProfileJoin.getLong("EMPLOYEE_ID"));
					employee.setUsername(employeesWithProfileJoin.getString("USERNAME"));
					employee.setProfile(profile);
					result.add(employee);
				}
				return result;
				
			}catch(SQLException e) {
				e.printStackTrace();
				log.error("SQL Exception at ManagerDao.getAllEmployees");
				return null;
			}
	}

	@Override
	//Will return all the requests that are not successfully removed
	public void changeStatusOfReimbursementRequest(List<ReimbursementRequest> requests) {
		for(int i = requests.size()-1; i >= 0; i--) {
			boolean success = changeStatusOfReimbursementRequest(requests.get(i));
			if(!success)return;
			requests.remove(i);
		}
	}
	
	private boolean changeStatusOfReimbursementRequest(ReimbursementRequest rr) {
		if(rr == null || rr.getId() <= 0 || rr.getApprovalReference() <= 0 || rr.getStatus() == null ) return false;
		String sql = "UPDATE REIMBURSEMENT SET STATUS = ?, APPROVER = ? WHERE REIMBURSEMENT_ID = ?";
		try(Connection connection = ConnectionUtil.getConnection();
			PreparedStatement pstmt = connection.prepareStatement(sql)){
			pstmt.setString(1, rr.getStatus().name());
			pstmt.setLong(2, rr.getApprovalReference());
			pstmt.setLong(3, rr.getId());
			return pstmt.executeUpdate() == 1;
		}catch(SQLException e) {
			e.printStackTrace();
			log.error("SQL Exception at ManagerDao.changeStatusOfReimbursementRequest argument:" + rr);
			
			return false;
		}
	}
	
	

}
