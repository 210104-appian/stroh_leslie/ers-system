package net.revature.leslie.stroh.dao;

public class EmployeeProfile {

	String firstName;
	String lastName;
	String title;
	long employeeReference;
	
	
	
	public EmployeeProfile() {
		//Default
	}



	public String getFirstName() {
		return firstName;
	}



	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}



	public String getLastName() {
		return lastName;
	}



	public void setLastName(String lastName) {
		this.lastName = lastName;
	}



	public String getTitle() {
		return title;
	}



	public void setTitle(String title) {
		this.title = title;
	}



	public long getEmployeeReference() {
		return employeeReference;
	}



	public void setEmployeeReference(long employeeReference) {
		this.employeeReference = employeeReference;
	}



	@Override
	public String toString() {
		return "EmployeeProfile [firstName=" + firstName + ", lastName=" + lastName + ", title=" + title
				+ ", employeeReference=" + employeeReference + "]";
	}
	
	
}
