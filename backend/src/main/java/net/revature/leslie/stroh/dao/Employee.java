package net.revature.leslie.stroh.dao;

import java.util.ArrayList;
import java.util.List;

public class Employee {
	
	

	long id;
	String username;
	String password;
	boolean isManager;
	List<ReimbursementRequest> requests;
	EmployeeProfile profile;
	


	public Employee() {
		//Default
		requests = new ArrayList<ReimbursementRequest>();
	}
	
	public Employee(long id) {
		this.id = id;
	}
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public boolean isManager() {
		return isManager;
	}

	public void setManager(boolean isManager) {
		this.isManager = isManager;
	}
	
	public List<ReimbursementRequest> getRequests() {
		return requests;
	}

	public void setRequests(List<ReimbursementRequest> requests) {
		this.requests = requests;
	}
	
	public EmployeeProfile getProfile() {
		return profile;
	}

	public void setProfile(EmployeeProfile profile) {
		this.profile = profile;
	}
	
	@Override
	public String toString() {
		return "Employee [id=" + id + ", username=" + username + ", password=" + password + ", isManager=" + isManager
				+ ", requests=" + requests + ", profile=" + profile + "]";
	}
	
	
}
