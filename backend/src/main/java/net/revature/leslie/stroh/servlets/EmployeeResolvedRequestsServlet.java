package net.revature.leslie.stroh.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.fasterxml.jackson.databind.ObjectMapper;

import net.revature.leslie.stroh.dao.ReimbursementRequest;
import net.revature.leslie.stroh.service.EmployeeService;

/*         GET /employee/view/resolved-request?={employee-id}          */
@SuppressWarnings("serial")
public class EmployeeResolvedRequestsServlet extends HttpServlet{

	private EmployeeService employeeService = new EmployeeService();
	private static Logger log = Logger.getRootLogger();
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		String id = request.getParameter("id");
		if(id == null) {
			log.info("A request was made to GET /employee/view/resolved-request without proper authorization parameter");
			response.setStatus(412);
			return;
		
		}
		List<ReimbursementRequest> pendingRequests = employeeService.getResolvedReimbursementRequests(id);
		if(pendingRequests == null) {
			response.setStatus(412);
		}
		ObjectMapper om = new ObjectMapper();
		String jsonItems = om.writeValueAsString(pendingRequests);
		PrintWriter pw = response.getWriter();
		pw.write(jsonItems);
		pw.close();
	}
}