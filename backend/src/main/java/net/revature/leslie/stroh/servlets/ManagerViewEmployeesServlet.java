package net.revature.leslie.stroh.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.fasterxml.jackson.databind.ObjectMapper;

import net.revature.leslie.stroh.dao.Employee;
import net.revature.leslie.stroh.service.ManagerService;

/*         GET manager/view/employees?={auth}       */
@SuppressWarnings("serial")
public class ManagerViewEmployeesServlet extends HttpServlet{
	private ManagerService managerService = new ManagerService();
	private static Logger log = Logger.getRootLogger();
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		String authorization = request.getParameter("auth");
		if(authorization == null ) {
			log.info("no authorization given for GET /manager/view/employees");
			response.setStatus(401);
			return;
		
		}
		List<Employee> employees = managerService.getAllEmployees(authorization);
		if(employees == null) {
			log.info("invalid authorization given for GET /manager/view/employees, authorization: " + authorization);
			response.setStatus(401);
			return;
		}
		
		ObjectMapper om = new ObjectMapper();
		String jsonItems = om.writeValueAsString(employees);
		PrintWriter pw = response.getWriter();
		pw.write(jsonItems);
		pw.close();
	}
}
