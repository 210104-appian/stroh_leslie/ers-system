package net.revature.leslie.stroh.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import net.revature.leslie.stroh.dao.Employee;
import net.revature.leslie.stroh.dao.EmployeeProfile;
import net.revature.leslie.stroh.dao.ReimbursementRequest;

public class EmployeeDaoImpl implements EmployeeDao{
	private static Logger log = Logger.getRootLogger();
	
	public boolean submitReimbursementRequest(ReimbursementRequest rr) {
		long id = getMax("SELECT MAX(REIMBURSEMENT_ID) FROM REIMBURSEMENT") + 1;
		String sql = "INSERT INTO REIMBURSEMENT VALUES(?,'PENDING',?,?,NULL,?)";
		try(Connection c = ConnectionUtil.getConnection();
			PreparedStatement pstmt = c.prepareStatement(sql)){
			pstmt.setLong(1,id);
			pstmt.setDouble(2, rr.getAmt());
			pstmt.setString(3,rr.getDescription());
			pstmt.setLong(4, rr.getRequestorReference());
			
			return pstmt.executeUpdate() == 1;
			
		}catch(SQLException e) {
			e.printStackTrace();
			log.error("SQL Exception at EmployeeDao.submitReimbursementRequest argument:" + rr);
			return false;
		}
	
	}
	
	@Override
	public List<ReimbursementRequest> getPendingReimbursementRequests(Employee employee) {
		return getReimbursementRequestsFromRequestorId(employee.getId(),"SELECT * FROM REIMBURSEMENT r WHERE r.REQUESTER = ? AND r.STATUS = 'PENDING'");
	}
	
	@Override
	public List<ReimbursementRequest> getResolvedReimbursementRequests(Employee employee) {
		return getReimbursementRequestsFromRequestorId(employee.getId(),"SELECT * FROM REIMBURSEMENT r WHERE r.REQUESTER = ? AND r.STATUS != 'PENDING'");
	}

	public EmployeeProfile getEmployeeProfile(Employee employee) {
		String sql = "SELECT * FROM PROFILE p WHERE p.PROFILE_OWNER = ?";
		try(Connection c = ConnectionUtil.getConnection();
				PreparedStatement pstmt = c.prepareStatement(sql)){
				pstmt.setLong(1, employee.getId());
				ResultSet rs = pstmt.executeQuery();
				EmployeeProfile result = new EmployeeProfile();
				for(boolean isValid = rs.next(); isValid ; isValid = rs.next()) {
					result.setFirstName(rs.getString("FIRST_NAME"));
					result.setLastName(rs.getString("LAST_NAME"));
					result.setTitle(rs.getString("TITLE"));
					result.setEmployeeReference(employee.getId());
				}
				return result;
				
			}catch(SQLException e) {
				e.printStackTrace();
				log.error("SQL Exception at EmployeeDao.getEmployeeProfile argument:" + employee);
				return null;
			}
	}

	
	
	private long getMax(String sql) {
		try(Connection connection = ConnectionUtil.getConnection();
				Statement stmt = connection.createStatement()	){
				ResultSet max = stmt.executeQuery(sql);
				if(!max.next()) return 1;
				else return max.getLong(1);
				
		}catch (SQLException e) {
			e.printStackTrace();
			log.error("SQL Exception at EmployeeDao.getMax");
		}
		return -1;
	}
	
	
	
	private List<ReimbursementRequest> getReimbursementRequestsFromRequestorId(long id,String sql) {
		try(Connection c = ConnectionUtil.getConnection();
				PreparedStatement pstmt = c.prepareStatement(sql)){
				pstmt.setLong(1, id);
				ResultSet rs = pstmt.executeQuery();
				ArrayList<ReimbursementRequest> result = new ArrayList<ReimbursementRequest>();
				for(boolean isValid = rs.next(); isValid ; isValid = rs.next()) {
					ReimbursementRequest request = new ReimbursementRequest();
					request.setId(rs.getLong("REIMBURSEMENT_ID"));
					request.setAmt(rs.getDouble("AMT"));
					request.setDescription(rs.getString("DESCRIPTION"));
					request.setRequestorReference(id);
					request.setApprovalReference(0);
					request.setStatus(ReimbursementRequest.Status.valueOf(rs.getString("STATUS")));
					result.add(request);
				}
				return result;
				
			}catch(SQLException e) {
				e.printStackTrace();
				log.error("SQL Exception at EmployeeDao.getReimbursementRequestsFromRequestorId argument:" +id + "," + sql);
				return null;
			}
	}

	@Override
	public boolean checkIfEmployeeExists(Employee e) {
		String sql = "SELECT * FROM EMPLOYEE WHERE EMPLOYEE_ID = ?";
		try(Connection connection = ConnectionUtil.getConnection();
		    PreparedStatement stmt = connection.prepareStatement(sql)){
			stmt.setLong(1, e.getId());
			ResultSet rs = stmt.executeQuery();
			if(!rs.next())return false;
			return true;
			
		} catch (SQLException e1) {
			e1.printStackTrace();
			log.error("SQL Exception at EmployeeDao.checkIfEmployeeExists argument:" +e);
			return false;
		}
		
		 
	}






}
