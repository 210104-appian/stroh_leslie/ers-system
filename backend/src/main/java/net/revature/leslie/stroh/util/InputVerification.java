package net.revature.leslie.stroh.util;

import java.util.regex.Pattern;

public class InputVerification {
	
	public static boolean isLong(String input) {
		return Pattern.matches("\\d+", input);
	}

}
