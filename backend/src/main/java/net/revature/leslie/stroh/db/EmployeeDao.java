package net.revature.leslie.stroh.db;
import java.util.List;

import net.revature.leslie.stroh.dao.*;
public interface EmployeeDao {
	
	boolean submitReimbursementRequest(ReimbursementRequest rr);
	List<ReimbursementRequest>getPendingReimbursementRequests(Employee e);
	List<ReimbursementRequest>getResolvedReimbursementRequests(Employee e);
	EmployeeProfile getEmployeeProfile(Employee e);
	boolean checkIfEmployeeExists(Employee e);
	
	

}
