package net.revature.leslie.stroh.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.log4j.Logger;

import net.revature.leslie.stroh.dao.Employee;

public class LoginDaoImpl implements LogInDao{
	private static Logger log = Logger.getRootLogger();
	@Override
	public Employee getEmployeeFromLoginCredentials(Employee employee) {
		String sql = "SELECT * FROM EMPLOYEE e WHERE e.USERNAME = ?";
		try(Connection connection = ConnectionUtil.getConnection();
		    PreparedStatement pstatement = connection.prepareStatement(sql)){
			pstatement.setString(1, employee.getUsername());
			ResultSet userSet = pstatement.executeQuery();
			for(boolean isValid = userSet.next(); isValid;) {
				//If password is incorrect immediately return null
				if(!employee.getPassword().equals(userSet.getString("PWD"))) return null;
				
				Employee queriedEmployee = new Employee();
				queriedEmployee.setId(userSet.getLong("EMPLOYEE_ID"));
				queriedEmployee.setManager(userSet.getString("IS_MANAGER").equals("Y"));
				return queriedEmployee;
			}
		}catch(SQLException e) {
			e.printStackTrace();
			log.error("SQL Exception at LoginDao.getEmployeeFromLoginCredentials argument:" + employee);
			return null;
		}
		//If the username does not exists it will end up here
		return null;
	}
}
		
