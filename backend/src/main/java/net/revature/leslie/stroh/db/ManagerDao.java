package net.revature.leslie.stroh.db;

import java.util.HashMap;
import java.util.List;

import net.revature.leslie.stroh.dao.Employee;
import net.revature.leslie.stroh.dao.ReimbursementRequest;

public interface ManagerDao {
    /*Fills the employee pending reimbursement requests*/
	List<Employee> getAllPendingReimbursements();
	
	/*Fills the employee resolved reimbursement requests*/
	List<Employee> getAllResolvedReimbursements();
	
	/*Fills employee information and profiles*/
	List<Employee> getAllEmployees();
	
	/*Consumes requests as it completes them, input list should be empty by completion */
	void changeStatusOfReimbursementRequest(List<ReimbursementRequest> requests);
	
	/*Checks only the ID of employee to see if it is a manager*/
	boolean checkIfManager(Employee e);
	
}
