package net.revature.leslie.stroh.service;

import net.revature.leslie.stroh.dao.Employee;
import net.revature.leslie.stroh.db.LogInDao;
import net.revature.leslie.stroh.db.LoginDaoImpl;

public class LogInService {
	LogInDao loginDao = new LoginDaoImpl();
	public Employee logIn(Employee employee) {
		return loginDao.getEmployeeFromLoginCredentials(employee);
	}
}
