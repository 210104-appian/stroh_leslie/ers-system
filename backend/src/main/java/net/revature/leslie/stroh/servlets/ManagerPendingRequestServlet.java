package net.revature.leslie.stroh.servlets;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import net.revature.leslie.stroh.dao.Employee;
import net.revature.leslie.stroh.dao.ReimbursementRequest;
import net.revature.leslie.stroh.service.ManagerService;
import net.revature.leslie.stroh.util.InputVerification;



/*         GET manager/view/pending-request?={auth}       */
/*		   PUT manager/view/pending-request?={auth}       */

@SuppressWarnings("serial")
public class ManagerPendingRequestServlet extends HttpServlet {
	private ManagerService managerService = new ManagerService();
	private static Logger log = Logger.getRootLogger();
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		String authorization = request.getParameter("auth");
		if(authorization == null ) {
			log.info("no authorization given for GET /manager/view/pending-request");
			response.setStatus(401);
			return;
		
		}
		List<Employee> pendingRequests = managerService.getAllEmployeesWithPendingReimbursementRequests(authorization);
		if(pendingRequests == null) {
			log.info("invalid authorization given for GET /manager/view/pending-request, authorization: " + authorization);
			response.setStatus(401);
			return;
		}
		
		ObjectMapper om = new ObjectMapper();
		String jsonItems = om.writeValueAsString(pendingRequests);
		PrintWriter pw = response.getWriter();
		pw.write(jsonItems);
		pw.close();
	}
	
	protected void doPut(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		String authorization = request.getParameter("auth");
		if(authorization == null){
			log.info("no authorization given for PUT /manager/view/pending-request");
			response.setStatus(401);
			return;
		}
		
		BufferedReader br = request.getReader();
		String json = "";
		for(String line = br.readLine(); line != null; line = br.readLine()) {
			json += line;
		}
		
		ObjectMapper om = new ObjectMapper();
		List<ReimbursementRequest> reimbursementRequests = om.readValue(json, new TypeReference<List<ReimbursementRequest>>() {});
		//The only reimbursement requests left in the reimbursement request lists are ones that have not been updated after this function.
		boolean hasPermission = managerService.updateReimbursementRequestStatus(authorization,reimbursementRequests);
		if(!hasPermission) {
			log.info("invalid authorization given for PUT /manager/view/pending-request, authorization: " + authorization);
			response.setStatus(401);
			return;
		}
		
		log.info("EmployeeId: " + authorization + " resolved requests: " + json);
		String jsonItems = om.writeValueAsString(reimbursementRequests);
		PrintWriter pw = response.getWriter();
		pw.write(jsonItems);
		pw.close();
	}
}
