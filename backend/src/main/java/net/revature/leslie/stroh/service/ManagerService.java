package net.revature.leslie.stroh.service;


import java.util.List;

import net.revature.leslie.stroh.dao.Employee;
import net.revature.leslie.stroh.dao.ReimbursementRequest;
import net.revature.leslie.stroh.db.ManagerDao;
import net.revature.leslie.stroh.db.ManagerDaoImpl;
import net.revature.leslie.stroh.util.InputVerification;

public class ManagerService {
	ManagerDao managerDao;
	
	public ManagerService() {
		this.managerDao =  new ManagerDaoImpl();
	}
	
	public List<Employee> getAllEmployeesWithPendingReimbursementRequests(String id){
		if(!InputVerification.isLong(id)) return null;
		if(managerDao.checkIfManager(new Employee(Long.valueOf(id)))){
			return managerDao.getAllPendingReimbursements();
		}
		return null;
	}
	
	public List<Employee> getAllEmployeesWithResolvedReimbursementRequests(String id){
		if(!InputVerification.isLong(id)) return null;
		if(managerDao.checkIfManager(new Employee(Long.valueOf(id)))){
			return managerDao.getAllResolvedReimbursements();
		}
		return null;
	}
	
	public boolean updateReimbursementRequestStatus(String id, List<ReimbursementRequest> rr){
		if(!InputVerification.isLong(id)) return false;
		if(managerDao.checkIfManager(new Employee(Long.valueOf(id)))){
		    managerDao.changeStatusOfReimbursementRequest(rr);
		    return true;
		}
		return false;
	}
	
	public List<Employee> getAllEmployees(String id){
		if(!InputVerification.isLong(id)) return null;
		if(managerDao.checkIfManager(new Employee(Long.valueOf(id)))){
			return managerDao.getAllEmployees();
		}
		return null;
	}
}
