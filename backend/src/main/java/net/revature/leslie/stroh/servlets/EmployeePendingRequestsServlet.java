package net.revature.leslie.stroh.servlets;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.fasterxml.jackson.databind.ObjectMapper;

import net.revature.leslie.stroh.dao.ReimbursementRequest;
import net.revature.leslie.stroh.service.EmployeeService;



/*         GET  /employee/view/request-resolved?={employee-id}          */
/*         POST /employee/view/request-pending?={employee-id}          */
@SuppressWarnings("serial")
public class EmployeePendingRequestsServlet extends HttpServlet{
	private static Logger log = Logger.getRootLogger();
	private EmployeeService employeeService = new EmployeeService();
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		String id = request.getParameter("id");
		if(id == null) {
			log.info("A request was made to GET /employee/view/pending-request without proper authorization parameter");
			response.setStatus(412);
			return;
		
		}
		List<ReimbursementRequest> pendingRequests = employeeService.getPendingReimbursementRequests(id);
		if(pendingRequests == null) {
			response.setStatus(412);
		}
		ObjectMapper om = new ObjectMapper();
		String jsonItems = om.writeValueAsString(pendingRequests);
		PrintWriter pw = response.getWriter();
		pw.write(jsonItems);
		pw.close();
	}
	
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		String id = request.getParameter("id");
		if(id == null) {
			log.info("A request was made to POST /employee/view/pending-request without proper authorization parameter");
			response.setStatus(412);
			return;
		}
		BufferedReader br = request.getReader();
		String json = "";
		for(String line = br.readLine(); line != null; line = br.readLine()) {
			json += line;
		}
		ObjectMapper om = new ObjectMapper();
		ReimbursementRequest reimbursementRequest = om.readValue(json, ReimbursementRequest.class);
		
		//The idea is to take care of data validation on the client side and only receive correct requests from the user
		boolean success = employeeService.submitReimbursementRequest(reimbursementRequest,id);
		if(success) {
			log.info("EmployeeID:" + id + " submitted new reimbursement request: " + json);
			response.setStatus(201);
		}
		else{
			log.error("EmployeeId:" + id + " failed to submit reimbursement request: " + json);
			response.sendError(412);
		}
	}
}
