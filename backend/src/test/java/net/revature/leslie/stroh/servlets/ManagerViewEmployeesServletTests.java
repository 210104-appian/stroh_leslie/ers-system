package net.revature.leslie.stroh.servlets;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import net.revature.leslie.stroh.dao.Employee;

public class ManagerViewEmployeesServletTests {
	@Test
	public void GETnoValidIdParameter() {
		String url = "http://localhost:8080/revature-project1/manager/view/employees";
		try {
			String response = SendHttpRequest.getRequest(url);
			Assert.fail();
		} catch (IOException e) {
			assertEquals("401", SendHttpRequest.parseResponseCodeFromException(e.getMessage()));
		}
	}
	
	@Test
	public void GETinvalidInputParameter() {
		String url = "http://localhost:8080/revature-project1/manager/view/employees?auth=bananas";
		try {
			String response = SendHttpRequest.getRequest(url);
			Assert.fail();
		} catch (IOException e) {
			assertEquals("401", SendHttpRequest.parseResponseCodeFromException(e.getMessage()));
		}
	}
	
	@Test
	public void GETinputIsNotManager() {
		String url = "http://localhost:8080/revature-project1/manager/view/employees?auth=1";
		try {
			String response = SendHttpRequest.getRequest(url);
			Assert.fail();
		} catch (IOException e) {
			assertEquals("401", SendHttpRequest.parseResponseCodeFromException(e.getMessage()));
		}
	}
	
	@Test
	public void GETgiveInputParameterThatDoesNotExist()  {
		String url = "http://localhost:8080/revature-project1/manager/view/employees?auth=0";
		try {
			String response = SendHttpRequest.getRequest(url);
			Assert.fail();
		} catch (IOException e) {
			assertEquals("401", SendHttpRequest.parseResponseCodeFromException(e.getMessage()));
		}
		
		
	}
	
	@Test
	public void GETgiveAValidInputParameter() throws IOException {
		String url = "http://localhost:8080/revature-project1/manager/view/employees?auth=4";
		String response = SendHttpRequest.getRequest(url);
		ObjectMapper om = new ObjectMapper();
		List<Employee> employees = om.readValue(response, new TypeReference<List<Employee>>() {});
		boolean found = false;
		for(Employee employee : employees) {
			if(employee.getId() == 5) {
				found = true;
				assertEquals("PMCCARTNEY",employee.getUsername());
				assertEquals("PAUL",employee.getProfile().getFirstName());
			}
		}
		if(!found) Assert.fail();
	}
	
	
	
	
}
