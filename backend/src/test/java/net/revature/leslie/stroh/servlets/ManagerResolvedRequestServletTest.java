package net.revature.leslie.stroh.servlets;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import net.revature.leslie.stroh.dao.Employee;
import net.revature.leslie.stroh.dao.ReimbursementRequest;

public class ManagerResolvedRequestServletTest {

	@Test
	public void GETnoValidIdParameter() {
		String url = "http://localhost:8080/revature-project1/manager/view/request-resolved";
		try {
			String response = SendHttpRequest.getRequest(url);
			Assert.fail();
		} catch (IOException e) {
			assertEquals("401", SendHttpRequest.parseResponseCodeFromException(e.getMessage()));
		}
	}
	
	@Test
	public void GETinvalidInputParameter() {
		String url = "http://localhost:8080/revature-project1/manager/view/request-resolved?auth=bananas";
		try {
			String response = SendHttpRequest.getRequest(url);
			Assert.fail();
		} catch (IOException e) {
			assertEquals("401", SendHttpRequest.parseResponseCodeFromException(e.getMessage()));
		}
	}
	
	@Test
	public void GETgiveInputParameterThatDoesNotExist() {
		String url = "http://localhost:8080/revature-project1/manager/view/request-resolved?auth=0";
		try {
			String response = SendHttpRequest.getRequest(url);
			Assert.fail();
		} catch (IOException e) {
			assertEquals("401", SendHttpRequest.parseResponseCodeFromException(e.getMessage()));
		}
		
		
	}
	@Test
	public void GETinputIsNotManager() {
		String url = "http://localhost:8080/revature-project1/manager/view/request-resolved?auth=1";
		try {
			String response = SendHttpRequest.getRequest(url);
			Assert.fail();
		} catch (IOException e) {
			assertEquals("401", SendHttpRequest.parseResponseCodeFromException(e.getMessage()));
		}
	}
	
	@Test
	public void GETgiveAValidInputParameter() throws IOException {
		String url = "http://localhost:8080/revature-project1/manager/view/request-resolved?auth=4";
		String response = SendHttpRequest.getRequest(url);
		ObjectMapper om = new ObjectMapper();
		List<Employee> employees = om.readValue(response, new TypeReference<List<Employee>>() {});
		Employee bobdylan = getTestEmployeeWithTestReimbursementRequest(employees);
		assertEquals("BDYLAN",bobdylan.getUsername());
		assertTrue(includesTestReimbursementRequest(bobdylan.getRequests()));
	}
	
	private Employee getTestEmployeeWithTestReimbursementRequest(List<Employee> employees) {
		for(Employee employee : employees) {
			if(employee.getId() == 1) {
				return employee;
			}
		}
		return null;
	}
	
	private boolean includesTestReimbursementRequest(List<ReimbursementRequest> requests) {
		for(ReimbursementRequest r : requests) {
			if(r.getId() == 23 
					&& r.getStatus() == ReimbursementRequest.Status.APPROVED
					&& r.getAmt() == 25
			        && r.getRequestorReference() == 1){
				return true;
			}
		}
		return false;
	}
}
