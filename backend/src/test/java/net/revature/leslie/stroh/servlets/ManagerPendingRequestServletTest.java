package net.revature.leslie.stroh.servlets;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.junit.Assert;
import org.junit.Test;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import net.revature.leslie.stroh.dao.Employee;
import net.revature.leslie.stroh.dao.ReimbursementRequest;

public class ManagerPendingRequestServletTest {
	@Test
	public void GETnoValidIdParameter() {
		String url = "http://localhost:8080/revature-project1/manager/view/request-pending";
		try {
			String response = SendHttpRequest.getRequest(url);
			Assert.fail();
		} catch (IOException e) {
			assertEquals("401", SendHttpRequest.parseResponseCodeFromException(e.getMessage()));
		}
	}
	
	@Test
	public void GETinvalidInputParameter() {
		String url = "http://localhost:8080/revature-project1/manager/view/request-pending?auth=bananas";
		try {
			String response = SendHttpRequest.getRequest(url);
			Assert.fail();
		} catch (IOException e) {
			assertEquals("401", SendHttpRequest.parseResponseCodeFromException(e.getMessage()));
		}
	}
	
	@Test
	public void GETgiveInputParameterThatDoesNotExist() {
		String url = "http://localhost:8080/revature-project1/manager/view/request-pending?auth=0";
		try {
			String response = SendHttpRequest.getRequest(url);
			Assert.fail();
		} catch (IOException e) {
			assertEquals("401", SendHttpRequest.parseResponseCodeFromException(e.getMessage()));
		}
		
		
	}
	@Test
	public void GETinputIsNotManager() {
		String url = "http://localhost:8080/revature-project1/manager/view/request-pending?auth=1";
		try {
			String response = SendHttpRequest.getRequest(url);
			Assert.fail();
		} catch (IOException e) {
			assertEquals("401", SendHttpRequest.parseResponseCodeFromException(e.getMessage()));
		}
	}
	
	@Test
	public void GETgiveAValidInputParameter() throws IOException {
		String url = "http://localhost:8080/revature-project1/manager/view/request-pending?auth=4";
		String response = SendHttpRequest.getRequest(url);
		ObjectMapper om = new ObjectMapper();
		List<Employee> employees = om.readValue(response, new TypeReference<List<Employee>>() {});
		Employee bobdylan = getTestEmployeeWithTestReimbursementRequest(employees);
		assertEquals("BDYLAN",bobdylan.getUsername());
		assertTrue(includesTestReimbursementRequest(bobdylan.getRequests()));
	}
	
	private Employee getTestEmployeeWithTestReimbursementRequest(List<Employee> employees) {
		for(Employee employee : employees) {
			if(employee.getId() == 1) {
				return employee;
			}
		}
		return null;
	}
	
	private boolean includesTestReimbursementRequest(List<ReimbursementRequest> requests) {
		for(ReimbursementRequest r : requests) {
			if(r.getId() == 30 
					&& r.getStatus() == ReimbursementRequest.Status.PENDING 
					&& r.getAmt() == 100
			        && r.getRequestorReference() == 1){
				return true;
			}
		}
		return false;
	}
	
	
	@Test
	public void PUTnoIdParameterProvided() {
		String url = "http://localhost:8080/revature-project1/manager/view/request-pending";
		
		try {
			SendHttpRequest.putRequest(url, new ArrayList<ReimbursementRequest>());
			Assert.fail();
		} catch (IOException e) {
			assertEquals("401", SendHttpRequest.parseResponseCodeFromException(e.getMessage()));
		}
	}
	
	@Test
	public void POSTInvalidIdParameterProvided() {
		String url = "http://localhost:8080/revature-project1/manager/view/request-pending?auth=banana";
		
		try {
			SendHttpRequest.putRequest(url, new ArrayList<ReimbursementRequest>());
			Assert.fail();
		} catch (IOException e) {
			assertEquals("401", SendHttpRequest.parseResponseCodeFromException(e.getMessage()));
		}
	}
	
	@Test
	public void POSTNonExistantIdParameterProvided() {
		String url = "http://localhost:8080/revature-project1/manager/view/request-pending?auth=0";
		try {
			SendHttpRequest.putRequest(url, new ArrayList<ReimbursementRequest>());
			Assert.fail();
		} catch (IOException e) {
			assertEquals("401", SendHttpRequest.parseResponseCodeFromException(e.getMessage()));
		}
	}
	
	@Test
	public void PUTinputIsNotManager() {
		String url = "http://localhost:8080/revature-project1/manager/view/request-pending?auth=1";
		try {
			
			String response = SendHttpRequest.putRequest(url,new ArrayList<ReimbursementRequest>());
			Assert.fail();
		} catch (IOException e) {
			assertEquals("401", SendHttpRequest.parseResponseCodeFromException(e.getMessage()));
		}
	}
	

	//This test does require a call to resolved request for manager as well. Most of the tests that change the database are not Unit tests.
	@Test
	public void POSTExistingIdParameterProvidedThenGET() throws IOException {
		String url = "http://localhost:8080/revature-project1/manager/view/request-pending?auth=4";
		Random rand = new Random();
		String testDescription = "test" + rand.nextDouble();
		ObjectMapper om = new ObjectMapper();
		
		//Create a new reimbursement request with post(Bob dylan id= 1,status PENDING);
	    createReimbursementRequestWithDescription(testDescription);
	    
	    //Find the created request
	    String json = SendHttpRequest.getRequest(url);
	    List<Employee> employeesWithPendingRequests = om.readValue(json, new TypeReference<List<Employee>>() {});
	    ReimbursementRequest created = getCreatedReimbursementRequest(employeesWithPendingRequests, testDescription);
		//Set approval reference appropriately
	    created.setApprovalReference(4);
	    //Send updated request to server via put
		created.setStatus(ReimbursementRequest.Status.REJECTED);

		List<ReimbursementRequest> requests = new ArrayList<ReimbursementRequest>();
		requests.add(created);
		SendHttpRequest.putRequest(url,requests);
		
		String response = SendHttpRequest.getRequest("http://localhost:8080/revature-project1/manager/view/request-resolved?auth=4");
		//Find request again and make sure it is updated
		employeesWithPendingRequests = om.readValue(response, new TypeReference<List<Employee>>() {});
		assertTrue(createdRequestIsUpdatedWithRejectedStatus(employeesWithPendingRequests,testDescription));
			
	}

	private boolean createdRequestIsUpdatedWithRejectedStatus(List<Employee> employees, String description) {
		for(Employee e : employees) {
			if(e.getId() == 1){
				List<ReimbursementRequest> requests = e.getRequests();
				for(ReimbursementRequest request : requests) {
					if(request.getDescription().equals(description)) {
						return request.getStatus() == ReimbursementRequest.Status.REJECTED;
					}
				}
			}
		}
		return false;
	}
	
	private ReimbursementRequest getCreatedReimbursementRequest(List<Employee> employees, String description) {
		for(Employee e : employees) {
			if(e.getId() == 1){
				List<ReimbursementRequest> requests = e.getRequests();
				for(ReimbursementRequest request : requests) {
					if(request.getDescription().equals(description)) {
						return request;
					}
				}
			}
		}
		return null;
	}

	
	private  void createReimbursementRequestWithDescription(String description) throws IOException {
		ReimbursementRequest rr = new ReimbursementRequest();
		rr.setStatus(ReimbursementRequest.Status.PENDING);
		rr.setAmt(0.00);
		rr.setRequestorReference(1);
		rr.setDescription(description);
		SendHttpRequest.postRequest("http://localhost:8080/revature-project1/employee/view/request-pending?id=1", rr);
	}
		
}
