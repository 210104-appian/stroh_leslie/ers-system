package net.revature.leslie.stroh.servlets;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.List;
import java.util.Random;

import org.junit.Assert;
import org.junit.Test;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import net.revature.leslie.stroh.dao.ReimbursementRequest;

public class EmployeePendingRequestsServletTests {
	
	
	@Test
	public void GETnoValidIdParameter() {
		String url = "http://localhost:8080/revature-project1/employee/view/request-pending";
		try {
			String response = SendHttpRequest.getRequest(url);
			Assert.fail();
		} catch (IOException e) {
			assertEquals("412", SendHttpRequest.parseResponseCodeFromException(e.getMessage()));
		}
	}
	
	@Test
	public void GETinvalidInputParameter() {
		String url = "http://localhost:8080/revature-project1/employee/view/request-pending?id=bananas";
		try {
			String response = SendHttpRequest.getRequest(url);
			Assert.fail();
		} catch (IOException e) {
			assertEquals("412", SendHttpRequest.parseResponseCodeFromException(e.getMessage()));
		}
	}
	
	@Test
	public void GETgiveInputParameterThatDoesNotExist() throws IOException {
		String url = "http://localhost:8080/revature-project1/employee/view/request-pending?id=0";
		String response = SendHttpRequest.getRequest(url);
		ObjectMapper om = new ObjectMapper();
		List<ReimbursementRequest> reimbursementRequests = om.readValue(response, new TypeReference<List<ReimbursementRequest>>() {});
		assertTrue(reimbursementRequests.isEmpty());
		
	}
	
	@Test
	public void GETgiveAValidInputParameter() throws IOException {
		String url = "http://localhost:8080/revature-project1/employee/view/request-pending?id=1";
		String response = SendHttpRequest.getRequest(url);
		ObjectMapper om = new ObjectMapper();
		List<ReimbursementRequest> reimbursementRequests = om.readValue(response, new TypeReference<List<ReimbursementRequest>>() {});
		assertTrue(includesTestReimbursementRequest(reimbursementRequests));
	}
	
	private boolean includesTestReimbursementRequest(List<ReimbursementRequest> requests) {
		for(ReimbursementRequest r : requests) {
			if(r.getId() == 30 
					&& r.getStatus() == ReimbursementRequest.Status.PENDING 
					&& r.getAmt() == 100
			        && r.getRequestorReference() == 1){
				return true;
			}
		}
		return false;
	}
	
	@Test
	public void POSTnoIdParameterProvided() {
		String url = "http://localhost:8080/revature-project1/employee/view/request-pending";
		
		try {
			SendHttpRequest.postRequest(url, new ReimbursementRequest());
			Assert.fail();
		} catch (IOException e) {
			assertEquals("412", SendHttpRequest.parseResponseCodeFromException(e.getMessage()));
		}
	}
	
	@Test
	public void POSTInvalidIdParameterProvided() {
		String url = "http://localhost:8080/revature-project1/employee/view/request-pending?id=banana";
		
		try {
			SendHttpRequest.postRequest(url, new ReimbursementRequest());
			Assert.fail();
		} catch (IOException e) {
			assertEquals("412", SendHttpRequest.parseResponseCodeFromException(e.getMessage()));
		}
	}
	
	@Test
	public void POSTNonExistantIdParameterProvided() {
		String url = "http://localhost:8080/revature-project1/employee/view/request-pending?id=0";
		try {
			SendHttpRequest.postRequest(url, new ReimbursementRequest());
			Assert.fail();
		} catch (IOException e) {
			assertEquals("412", SendHttpRequest.parseResponseCodeFromException(e.getMessage()));
		}
	}
	
	@Test
	public void POSTrequestorIDnotEqualToInputId(){
		String url = "http://localhost:8080/revature-project1/employee/view/request-pending?id=1";
		Random rand = new Random();
		String testDescription = "test" + rand.nextDouble();
		ReimbursementRequest rr = new ReimbursementRequest();
			rr.setStatus(ReimbursementRequest.Status.PENDING);
			rr.setAmt(0.00);
			rr.setDescription(testDescription);
		try {
			SendHttpRequest.postRequest(url, rr);
			Assert.fail();
		} catch (IOException e) {
			assertEquals("412", SendHttpRequest.parseResponseCodeFromException(e.getMessage()));
		}
			
	}
	
	@Test
	public void POSTExistingIdParameterProvidedThenGET() throws IOException {
		String url = "http://localhost:8080/revature-project1/employee/view/request-pending?id=1";
		Random rand = new Random();
		String testDescription = "test" + rand.nextDouble();
		ReimbursementRequest rr = new ReimbursementRequest();
			rr.setStatus(ReimbursementRequest.Status.PENDING);
			rr.setAmt(0.00);
			rr.setRequestorReference(1);
			rr.setDescription(testDescription);
		SendHttpRequest.postRequest(url, rr);

		String response = SendHttpRequest.getRequest(url);
		ObjectMapper om = new ObjectMapper();
		List<ReimbursementRequest> reimbursementRequests = om.readValue(response, new TypeReference<List<ReimbursementRequest>>() {});
		assertTrue(includesTestDescriptionReimbursementRequest(reimbursementRequests,testDescription));
		
	}

	private boolean includesTestDescriptionReimbursementRequest(List<ReimbursementRequest> requests,String description) {
		for(ReimbursementRequest r : requests) {
			if(r.getDescription().equals(description)){
				return true;
			}
		}
		return false;
	}
		
	
	
}
