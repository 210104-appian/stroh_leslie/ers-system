package net.revature.leslie.stroh.servlets;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import net.revature.leslie.stroh.dao.ReimbursementRequest;

public class EmployeeResolvedRequestsServletTests {

	@Test
	public void GETnoValidIdParameter() {
		String url = "http://localhost:8080/revature-project1/employee/view/request-resolved";
		try {
			String response = SendHttpRequest.getRequest(url);
			Assert.fail();
		} catch (IOException e) {
			assertEquals("412", SendHttpRequest.parseResponseCodeFromException(e.getMessage()));
		}
	}
	
	@Test
	public void GETinvalidInputParameter() {
		String url = "http://localhost:8080/revature-project1/employee/view/request-resolved?id=bananas";
		try {
			String response = SendHttpRequest.getRequest(url);
			Assert.fail();
		} catch (IOException e) {
			assertEquals("412", SendHttpRequest.parseResponseCodeFromException(e.getMessage()));
		}
	}
	
	@Test
	public void GETgiveInputParameterThatDoesNotExist() throws IOException {
		String url = "http://localhost:8080/revature-project1/employee/view/request-resolved?id=0";
		String response = SendHttpRequest.getRequest(url);
		ObjectMapper om = new ObjectMapper();
		List<ReimbursementRequest> reimbursementRequests = om.readValue(response, new TypeReference<List<ReimbursementRequest>>() {});
		assertTrue(reimbursementRequests.isEmpty());
		
	}
	
	@Test
	public void GETgiveAValidInputParameter() throws IOException {
		String url = "http://localhost:8080/revature-project1/employee/view/request-resolved?id=1";
		String response = SendHttpRequest.getRequest(url);
		ObjectMapper om = new ObjectMapper();
		List<ReimbursementRequest> reimbursementRequests = om.readValue(response, new TypeReference<List<ReimbursementRequest>>() {});
		assertTrue(includesTestReimbursementRequest(reimbursementRequests));
	}
	
	private boolean includesTestReimbursementRequest(List<ReimbursementRequest> requests) {
		for(ReimbursementRequest r : requests) {
			if(r.getId() == 25 
					&& r.getStatus() == ReimbursementRequest.Status.APPROVED
					&& r.getAmt() == 50
			        && r.getRequestorReference() == 1){
				return true;
			}
		}
		return false;
	}
}
