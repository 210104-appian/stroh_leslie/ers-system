package net.revature.leslie.stroh.servlets;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import net.revature.leslie.stroh.dao.EmployeeProfile;
import net.revature.leslie.stroh.dao.ReimbursementRequest;

public class EmployeeProfileServletTests {

	
	
	@Test
	public void GETnoValidIdParameter() {
		String url = "http://localhost:8080/revature-project1/employee/profile";
		try {
			String response = SendHttpRequest.getRequest(url);
			Assert.fail();
		} catch (IOException e) {
			assertEquals("412", SendHttpRequest.parseResponseCodeFromException(e.getMessage()));
		}
	}
	
	@Test
	public void GETinvalidInputParameter() {
		String url = "http://localhost:8080/revature-project1/employee/profile?id=bananas";
		try {
			String response = SendHttpRequest.getRequest(url);
			Assert.fail();
		} catch (IOException e) {
			assertEquals("412", SendHttpRequest.parseResponseCodeFromException(e.getMessage()));
		}
	}
	
	@Test
	public void GETgiveInputParameterThatDoesNotExist() throws IOException {
		String url = "http://localhost:8080/revature-project1/employee/profile?id=0";
		String response = SendHttpRequest.getRequest(url);
		ObjectMapper om = new ObjectMapper();
		EmployeeProfile profile = om.readValue(response,EmployeeProfile.class);
		assertTrue(profile.getFirstName()==null);
		assertTrue(profile.getLastName()==null);
		assertTrue(profile.getTitle()==null);

		
	}
	
	@Test
	public void GETgiveAValidInputParameter() throws IOException {
		String url = "http://localhost:8080/revature-project1/employee/profile?id=1";
		String response = SendHttpRequest.getRequest(url);
		ObjectMapper om = new ObjectMapper();
		EmployeeProfile profile = om.readValue(response,EmployeeProfile.class);
		assertEquals("BOB",profile.getFirstName());
		assertEquals("DYLAN",profile.getLastName());
		assertEquals("CASHIER",profile.getTitle());
	}
}
