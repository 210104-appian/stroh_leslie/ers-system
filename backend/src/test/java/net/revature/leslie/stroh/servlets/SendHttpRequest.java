package net.revature.leslie.stroh.servlets;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.databind.ObjectMapper;

import net.revature.leslie.stroh.dao.Employee;
import net.revature.leslie.stroh.dao.ReimbursementRequest;

public class SendHttpRequest {

	
	/*https://stackabuse.com/how-to-send-http-requests-in-java/ --- Pretty much just took it */
 static String postRequest(String requesturl,Employee toSend) throws IOException {
	URL url = new URL(requesturl);
	HttpURLConnection connection = (HttpURLConnection) url.openConnection();
	connection.setRequestMethod("POST");
	connection.setRequestProperty("Content-Type", "application/json");
	
	ObjectMapper om = new ObjectMapper();
	byte[] json = om.writeValueAsBytes(toSend);
	connection.setDoOutput(true);
	try (DataOutputStream writer = new DataOutputStream(connection.getOutputStream())) {
	    writer.write(json);
	    writer.flush();
	    writer.close();
	    
	    
	    StringBuilder content = new StringBuilder();
	    try(BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()))){
	    	String line;
	    	while ((line = in.readLine()) != null) {
                content.append(line);
                content.append(System.lineSeparator());
            }
	    }
	    return content.toString();
	}finally{
		connection.disconnect();
	}
	
}
	 
 static String postRequest(String requesturl,ReimbursementRequest toSend) throws IOException {
	URL url = new URL(requesturl);
	HttpURLConnection connection = (HttpURLConnection) url.openConnection();
	connection.setRequestMethod("POST");
	connection.setRequestProperty("Content-Type", "application/json");
	
	ObjectMapper om = new ObjectMapper();
	byte[] json = om.writeValueAsBytes(toSend);
	connection.setDoOutput(true);
	try (DataOutputStream writer = new DataOutputStream(connection.getOutputStream())) {
	    writer.write(json);
	    writer.flush();
	    writer.close();
	    
	    
	    StringBuilder content = new StringBuilder();
	    try(BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()))){
	    	String line;
	    	while ((line = in.readLine()) != null) {
                content.append(line);
                content.append(System.lineSeparator());
            }
	    }
	    return content.toString();
	}finally{
		connection.disconnect();
	}
	
}
	 
 static String getRequest(String requesturl) throws IOException {
		URL url = new URL(requesturl);
		HttpURLConnection connection = (HttpURLConnection) url.openConnection();
		connection.setRequestMethod("GET");
		connection.setRequestProperty("Content-Type", "application/json");
		
		 StringBuilder content = new StringBuilder();
		 try(BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()))){
		    	String line;
		    	while ((line = in.readLine()) != null) {
	                content.append(line);
	                content.append(System.lineSeparator());
	            }
		 }
		 return content.toString();
	}
 
 static String putRequest(String requesturl, List<ReimbursementRequest> toSend) throws IOException {
		URL url = new URL(requesturl);
		HttpURLConnection connection = (HttpURLConnection) url.openConnection();
		connection.setRequestMethod("PUT");
		connection.setRequestProperty("Content-Type", "application/json");
		
		ObjectMapper om = new ObjectMapper();
		byte[] json = om.writeValueAsBytes(toSend);
		connection.setDoOutput(true);
		try (DataOutputStream writer = new DataOutputStream(connection.getOutputStream())) {
		    writer.write(json);
		    writer.flush();
		    writer.close();
		    
		    
		    StringBuilder content = new StringBuilder();
		    try(BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()))){
		    	String line;
		    	while ((line = in.readLine()) != null) {
	                content.append(line);
	                content.append(System.lineSeparator());
	            }
		    }
		    return content.toString();
		}finally{
			connection.disconnect();
		}
		
	}
 
	static String parseResponseCodeFromException(String message) {
		String[] split = message.split("\\s");
		for(int i = 0; i < split.length; i++) {
			if(split[i].equals("code:")) {
				return split[i+1];
			}
		}
		return "";
		
	}
	
}
