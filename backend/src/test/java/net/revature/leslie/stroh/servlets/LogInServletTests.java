package net.revature.leslie.stroh.servlets;

import static org.junit.Assert.assertEquals;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

import org.junit.Assert;
import org.junit.Test;

import com.fasterxml.jackson.databind.ObjectMapper;

import net.revature.leslie.stroh.dao.Employee;



public class LogInServletTests {

	
	@Test
	public void testInvalidUsernameTest() {
		String url = "http://localhost:8080/revature-project1/login";
		Employee employee = new Employee();
		employee.setUsername("BDYLA");
		employee.setPassword("LIKEAROLLINGSTONE");
		
		try {
			SendHttpRequest.postRequest(url,employee);
			Assert.fail("Should throw an exception");
		}catch(Exception e) {
			String code = SendHttpRequest.parseResponseCodeFromException(e.getMessage()).trim();
			assertEquals("401",code);
		}
	}
	
	@Test
	public void invalidPasswordTest() {
		String url = "http://localhost:8080/revature-project1/login";
		Employee employee = new Employee();
		employee.setUsername("BDYLAN");
		employee.setPassword("THis isn't right");
		
		try {
			SendHttpRequest.postRequest(url,employee);
			Assert.fail("Should throw an exception");
		}catch(Exception e) {
			
			String code = SendHttpRequest.parseResponseCodeFromException(e.getMessage()).trim();
			assertEquals("401",code);
		}
	}
	
	@Test
	public void testValidCredentials() throws IOException {
		String url = "http://localhost:8080/revature-project1/login";
		Employee employee = new Employee();
		employee.setUsername("BDYLAN");
		employee.setPassword("LIKEAROLLINGSTONE");
		String response = SendHttpRequest.postRequest(url,employee);
		ObjectMapper om = new ObjectMapper();
		Employee e = om.readValue(response, Employee.class);
		assertEquals(1,e.getId());
		
	}
	
	
	
}
