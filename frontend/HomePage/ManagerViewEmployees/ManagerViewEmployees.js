window.onload = function(){
    const token = sessionStorage.getItem("PlutoERSAuth");
    if(!token){
        window.location.replace("../../signIn.html");
    }
    insertHeader(token.split(":")[1] === 'MANAGER');
    fillEmployeeTable();

};

function getAuthFromSessionStorage(){
    const token = sessionStorage.getItem("PlutoERSAuth");
    if(token) return token.split(':')[0];
    else return undefined;
}


function fillEmployeeTable(){
   const auth = getAuthFromSessionStorage();
   if(auth){
     performAjaxGetRequest("http://localhost:8080/revature-project1/manager/view/employees?auth=" + auth,
                           fillInTableFromJson);
   }
   
}




function fillInTableFromJson(json){
    const employees = JSON.parse(json);
    const tableBody = document.getElementById("employeeTableBody");
    let htmlRows = employees.map((employee) =>{
        return `
        <tr>
           <td>${employee.id}</td>
           <td>${employee.username}</td>
           <td>${employee.profile.firstName}</td>
           <td>${employee.profile.lastName}</td>
           <td>${employee.profile.title}</td>
        </tr>
        ` 
    })
    tableBody.innerHTML = htmlRows.join();
}


function performAjaxGetRequest(url, successCallback){
    const xhr = new XMLHttpRequest();

    xhr.open("GET", url);

    xhr.onreadystatechange = function(){
        if(xhr.readyState==4){
            if(xhr.status >= 200 && xhr.status < 300 ){
                successCallback(xhr.response); // this is going to be the response body of our http response  (JSON)
            } else {
                console.log(xhr.status);
                console.error("something went wrong with our GET request to "+url);
            }
        } 
    }
    xhr.send();
}