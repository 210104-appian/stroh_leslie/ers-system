window.onload = function(){
    const token = sessionStorage.getItem("PlutoERSAuth");
    if(!token){
        console.log("made it here somehow");
        console.log(sessionStorage);
        window.location.replace("../../signIn.html");
    }
    insertHeader(token.split(":")[1] === 'MANAGER');
    makeHomeButtonLogout();
    fillNameIn();

    const logOutButton = document.getElementById("logOutButton");
    logOutButton.addEventListener("click",onClickLogOut);
    
};

function getAuthFromSessionStorage(){
    const token = sessionStorage.getItem("PlutoERSAuth");
    if(token) return token.split(':')[0];
    else return undefined;
}

function fillNameIn(){
    const id = getAuthFromSessionStorage();
    if(id) performAjaxGetRequest("http://localhost:8080/revature-project1/employee/profile?id=" + id,
                                 onSuccessfulQueryForProfile)
}

function onSuccessfulQueryForProfile(jsonResponse){
    const profile = JSON.parse(jsonResponse);
    const firstNameHtmlElement = document.getElementById("loggedInUserFirstName");
    firstNameHtmlElement.innerText = profile.firstName;
}

function makeHomeButtonLogout(){
    const homeButton = document.getElementById("NavHomeButton");
    homeButton.innerHTML = `<a type = "button" class="btn btn-link" id="logOutButton">Log Out</a>`
}

function onClickLogOut(){
    sessionStorage.removeItem("PlutoERSAuth");
    window.location.replace("../../signIn.html");
}

function performAjaxGetRequest(url, successCallback){
    const xhr = new XMLHttpRequest();
    
    xhr.open("GET", url);

    xhr.onreadystatechange = function(){
        if(xhr.readyState==4){
            if(xhr.status >= 200 && xhr.status < 300 ){
                successCallback(xhr.response); // this is going to be the response body of our http response  (JSON)
            } else {
                console.log(xhr.status);
                console.error("something went wrong with our GET request to "+url);
            }
        } 
    }
    xhr.send();
}