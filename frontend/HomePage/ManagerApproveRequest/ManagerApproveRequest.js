window.onload = function(){
    const token = sessionStorage.getItem("PlutoERSAuth");
    if(!token){
        window.location.replace("../../signIn.html");
    }
    insertHeader(token.split(":")[1] === 'MANAGER');
    hideApproveAndRejectButtons();
};

const auth = sessionStorage.getItem("PlutoERSAuth");
addEventClickListeners(auth.split(':')[0]);


function addEventClickListeners(employeeId){
    let elementPendingAction = document.getElementById("pendingAction");
    elementPendingAction.addEventListener("click", () => actionOnPendingSelect(employeeId));

    let elementResolvedAction = document.getElementById("resolvedAction");
    elementResolvedAction.addEventListener("click", () => actionOnResolvedSelect(employeeId));

    let approveButton = document.getElementById("Approve");
    approveButton.addEventListener("click", () => actionOnApproveButtonClick(employeeId));

    let rejectButton = document.getElementById("Reject");
    rejectButton.addEventListener("click",() => actionOnRejectButtonClick(employeeId));

    let modalCloseButton = document.getElementById("responseModal");
    modalCloseButton.addEventListener("click", closeModal);
}

function actionOnPendingSelect(employeeId){
    showApproveAndRejectButtons();
    let statusMenu = document.getElementById("statusMenu");
    showCurrentPendingRequests(employeeId);
    statusMenu.innerText = "Pending";
}
function actionOnResolvedSelect(employeeId){
    hideApproveAndRejectButtons();
    let statusMenu = document.getElementById("statusMenu");
    performAjaxGetRequest("http://localhost:8080/revature-project1/manager/view/request-resolved"+"?auth="+employeeId,
                          (json) =>  fillInManagerTableFromJson(json));
    statusMenu.innerText= "Resolved";
}

function actionOnApproveButtonClick(employeeId){
    updateCurrentlySelectedRequests(employeeId,"APPROVED",putResponseSuccessCallback,putResponseFailureCallback);
    openModal();
}

function actionOnRejectButtonClick(employeeId){
    updateCurrentlySelectedRequests(employeeId,"REJECTED",putResponseSuccessCallback,putResponseFailureCallback);
    openModal();  
}

function putResponseSuccessCallback(employeeId,payload,response){
    //Need to refresh the page on a successful callback, no need on a failed callback
    showCurrentPendingRequests(employeeId);
    const responseText = document.getElementById("responseModalText");
    let responseList = JSON.parse(response);
    let payLoadList = JSON.parse(payload);
    if(responseList.length === 0){
        responseText.innerHTML = `<span style="color: green">Successfully updated ALL ${payLoadList.length} 
        requests </span>`;
    }else{
        responseText.innerHTML = `<span style="color: red">Updated ${payLoadList.length - responseList.length} of ${payLoadList.length} Requests. Failed to update request ids:${responseList}`;
    }
    
}

function putResponseFailureCallback(payload, responseStatus){
    const responseText = document.getElementById("responseModalText");
    let payLoadList = JSON.parse(payload);
    responseText.innerHTML = `<span style="color: red">Failed to update all ${payLoadList.length} due to response status ${responseStatus}</span>`
}

function showCurrentPendingRequests(employeeId){
    performAjaxGetRequest("http://localhost:8080/revature-project1/manager/view/request-pending"+"?auth="+employeeId,
                           (json) =>  fillInManagerTableFromJson(json,true));
}

function fillInManagerTableFromJson(json,pending){
    const requests = JSON.parse(json);
    const tableBody = document.getElementById("reimbursementTableBody");
    const htmlRows = requests.map((request) =>{
        const reimbursementRequests = request.requests.map(reimbursement =>{
               return (pending) ? `<tr>
                 <td>${request.id}</td>
                 <td>${request.username}</td>
                 <td>${reimbursement.id}</td>
                 <td>${reimbursement.status}</td>
                 <td>${reimbursement.amt.toFixed(2)}</td>
                 <td>${reimbursement.description}</td>
                 <td>'Not Approved'</td>
                 <td><input class="form-check-input" type="checkbox" id="checkBoxReimbursement-${reimbursement.id}"> </input></td>
                 <tr>
                `:`<tr>
                <td>${request.id}</td>
                <td>${request.username}</td>
                <td>${reimbursement.id}</td>
                <td>${reimbursement.status}</td>
                <td>${reimbursement.amt.toFixed(2)}</td>
                <td>${reimbursement.description}</td>
                <td>${reimbursement.approvalReference}</td>
                <tr>
                `
        });
        return reimbursementRequests.join();
    });
    tableBody.innerHTML = htmlRows.join();
}

function updateCurrentlySelectedRequests(employeeId,newStatus,successCallback,failureCallback){
    const checkBoxes = document.getElementsByClassName("form-check-input");
    let checkedBoxes = [];
    for(let checkBox of checkBoxes){
        if(checkBox.checked){
            checkedBoxes.push(checkBox);
        }
    }

    const reimbursementRequests = checkedBoxes.map(checked =>{
        const id = checked.id.split('-')[1];
        const status = newStatus;
        const approvalReference = employeeId;
        return{id,status,approvalReference};
    });
    const payload = JSON.stringify(reimbursementRequests);
    performAjaxPutRequest(employeeId,"http://localhost:8080/revature-project1/manager/view/request-pending"+"?auth="+ employeeId,
                           payload,successCallback,failureCallback);
}

//Has to have a nested get request if successful so I had to include employeeId for the successful callback
function performAjaxPutRequest(employeeId, url, payload, successCallback, failureCallback){
    const xhr = new XMLHttpRequest();
    xhr.open("PUT", url);
    xhr.onreadystatechange = function(){
        if(xhr.readyState==4 ){
            if(xhr.status>199 && xhr.status<300){
                successCallback(employeeId,payload,xhr.response); // this is going to be the response body of our http response  (JSON)
            } else {
                if(failureCallback){
                    failureCallback(payload,xhr.status);
                } else{
                    console.error("An error occurred while attempting to create a new record")
                }
            }
        }
    }
    xhr.send(payload);
}

function performAjaxGetRequest(url, successCallback){
    const xhr = new XMLHttpRequest();

    xhr.open("GET", url);

    xhr.onreadystatechange = function(){
        if(xhr.readyState==4){
            if(xhr.status >= 200 && xhr.status < 300 ){
                successCallback(xhr.response); // this is going to be the response body of our http response  (JSON)
            } else {
                console.log(xhr.status);
                console.error("something went wrong with our GET request to "+url);
            }
        } 
    }
    xhr.send();
}

function hideApproveAndRejectButtons(){
    const buttons = document.getElementById("ApproveAndRejectButtons").children;
    for(let button of buttons){
        button.style.display = "none";
    }
}

function showApproveAndRejectButtons(){
    const buttons = document.getElementById("ApproveAndRejectButtons").children;
    for(let button of buttons){
        button.style.display = "";
    }
}

function openModal() {
    $("#responseModal").modal("show");
  }
  
  function closeModal() {
    $("#responseModal").modal("hide");
    const modalText = document.getElementById("responseModalText");
    modalText.innerText = "";
  }
