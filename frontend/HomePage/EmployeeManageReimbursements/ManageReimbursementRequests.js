



window.onload = function(){
    const token = sessionStorage.getItem("PlutoERSAuth");
    if(!token){
        window.location.replace("../../signIn.html");
    }
    insertHeader(token.split(":")[1] === 'MANAGER');
    
};


let auth = sessionStorage.getItem("PlutoERSAuth"); //TODO only here for testing purposes will get ID from session after a login
addEventClickListeners(auth.split(':')[0]);



function addEventClickListeners(employeeId){
    let elementPendingAction = document.getElementById("pendingAction");
    elementPendingAction.addEventListener("click", () => actionOnPendingSelect(employeeId));

    let elementResolvedAction = document.getElementById("resolvedAction");
    elementResolvedAction.addEventListener("click", () => actionOnResolvedSelect(employeeId));

    let submitNewRequestBtn = document.getElementById("addNewRequestButton");
    submitNewRequestBtn.addEventListener("click",function(){
        const failureText = document.getElementById("submitFailureText");
        failureText.style.display = "none";
        openModal();
    });

    let modalSubmitBtn = document.getElementById("submit-btn");
    modalSubmitBtn.addEventListener("click", () => submitNewRequest(employeeId, onSubmitRequestSuccess,onSubmitRequestFailure));

    let modalCloseBtn = document.getElementById("close-btn");
    modalCloseBtn.addEventListener("click",() =>{
        document.getElementById("amount-input").value = '';
        document.getElementById("description-input").value = '';
        closeModal();
        
    });
}

function actionOnPendingSelect(employeeId){
    let statusMenu = document.getElementById("statusMenu");
    performAjaxGetRequest("http://localhost:8080/revature-project1/employee/view/request-pending"+"?id="+employeeId,
                           (json) =>  fillInTableFromJson(json));
    statusMenu.innerText = "Pending";
}
function actionOnResolvedSelect(employeeId){
    let statusMenu = document.getElementById("statusMenu");
    performAjaxGetRequest("http://localhost:8080/revature-project1/employee/view/request-resolved"+"?id="+employeeId,
                          (json) =>  fillInTableFromJson(json));
    statusMenu.innerText= "Resolved";
}

function onSubmitRequestSuccess(){
    document.getElementById("amount-input").value = '';
    document.getElementById("description-input").value = '';
    closeModal();
}

function onSubmitRequestFailure(text){
   let failureText =  document.getElementById("submitFailureText");
   failureText.innerText = text;
   failureText.style.display = "block";
   setTimeout(()=>failureText.style.display = "none",3000);
}

function submitNewRequest(employeeId,successCallback,failureCallback){
    let amtValue = document.getElementById("amount-input").value;
    let description = document.getElementById("description-input").value;
    if(amtValue.length === 0 || isNaN(amtValue) || description.length === 0){
        failureCallback("Invalid input(s), amount must be a number and description must be at least 1 character");
        return;
    }
    amtValue = (+amtValue).toFixed(2);
    
    let object = {amt:amtValue,requestorReference:employeeId, description:description};
    let payload = JSON.stringify(object);
    performAjaxPostRequest("http://localhost:8080/revature-project1/employee/view/request-pending"+"?id="+employeeId,
                            payload,
                            successCallback,
                            failureCallback);
}


function fillInTableFromJson(json){
    const requests = JSON.parse(json);
    const tableBody = document.getElementById("reimbursementTableBody");
    let htmlRows = requests.map((request) =>{
        return `
        <tr>
           <td>${request.id}</td>
           <td>${request.status}</td>
           <td>${"$" + request.amt.toFixed(2)}</td>
           <td>${request.description}</td>
        </tr>
        ` 
    })
    tableBody.innerHTML = htmlRows.join();
}


function openModal() {
    $("#submitRequestModal").modal("show");
  }
  
  function closeModal() {
    $("#submitRequestModal").modal("hide");
  }

  function performAjaxPostRequest(url, payload, successCallback, failureCallback){
    const xhr = new XMLHttpRequest();
    xhr.open("POST", url);
    xhr.onreadystatechange = function(){
        if(xhr.readyState==4 ){
            if(xhr.status>199 && xhr.status<300){
                successCallback(xhr.response); // this is going to be the response body of our http response  (JSON)
            } else {
                if(failureCallback){
                    failureCallback("Failed to update on the server")
                } else{
                    console.error("An error occurred while attempting to create a new record")
                }
            }
        }
    }
    xhr.send(payload);
}

function performAjaxGetRequest(url, successCallback){
    const xhr = new XMLHttpRequest();
    
    xhr.open("GET", url);

    xhr.onreadystatechange = function(){
        if(xhr.readyState==4){
            if(xhr.status >= 200 && xhr.status < 300 ){
                successCallback(xhr.response); // this is going to be the response body of our http response  (JSON)
            } else {
                console.log(xhr.status);
                console.error("something went wrong with our GET request to "+url);
            }
        } 
    }
    xhr.send();
}