

/*This depends on only having a single header*/
function insertHeader(isManager){
    let header = `
    <nav class="navbar navbar-default">
    <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand">Pluto ERS System</a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
        <ul class="nav navbar-nav" id = "navbarLeftHandSide">
        <li><a href="../EmployeeManageReimbursements/ManageReimbursementRequests.html">Manage Reimbursement Requests</a></li>
        <li><a href="../EmployeeProfileView/EmployeeProfileView.html">View Profile</a></li>  
        </ul>
        <ul class="nav navbar-nav navbar-right">
            <li id = "NavHomeButton"><a href="../EmployeeHomePage/EmployeeHomePage.html">Back Home</a></li>
        </ul>
    </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
    </nav>`
    let elements = document.getElementsByTagName("header");
    elements.item(0).innerHTML= header;  
    if(isManager){
        addManagerTabs();
    }
}

function addManagerTabs(){
    const navBar = document.getElementById("navbarLeftHandSide");
    const approveRequestTab = document.createElement("li");
    const viewEmployeeTab = document.createElement("li");
    approveRequestTab.innerHTML = `<a href="../ManagerApproveRequest/ManagerApproveRequest.html">Approve Reimbursement Requests</a>`
    viewEmployeeTab.innerHTML = `<a href="../ManagerViewEmployees/ManagerViewEmployees.html">View Employees</a>`
    navBar.append(approveRequestTab,viewEmployeeTab);
}
