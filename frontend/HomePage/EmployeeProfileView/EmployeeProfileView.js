

window.onload = function(){
    const token = sessionStorage.getItem("PlutoERSAuth");
    if(!token){
        window.location.replace("../../signIn.html");
    }
    insertHeader(token.split(":")[1] === 'MANAGER');
    fillProfileIn();
};



function getAuthFromSessionStorage(){
    const token = sessionStorage.getItem("PlutoERSAuth");
    if(token) return token.split(':')[0];
    else return undefined;
}

function fillProfileIn(){
    const id = getAuthFromSessionStorage();
    if(id) performAjaxGetRequest("http://localhost:8080/revature-project1/employee/profile?id=" + id,
                                 onSuccessfulQueryForProfile)
}


function onSuccessfulQueryForProfile(jsonResponse){
    const profile = JSON.parse(jsonResponse);
    const firstNameHtmlElement = document.getElementById("profileFirstName");
    const lastNameHtmlElement = document.getElementById("profileLastName");
    const titleHtmlElement = document.getElementById("profileTitle");
    firstNameHtmlElement.value = profile.firstName;
    lastNameHtmlElement.value = profile.lastName;
    titleHtmlElement.value = profile.title;
}

function performAjaxGetRequest(url, successCallback){
    const xhr = new XMLHttpRequest();
    
    xhr.open("GET", url);

    xhr.onreadystatechange = function(){
        if(xhr.readyState==4){
            if(xhr.status >= 200 && xhr.status < 300 ){
                successCallback(xhr.response); // this is going to be the response body of our http response  (JSON)
            } else {
                console.log(xhr.status);
                console.error("something went wrong with our GET request to "+url);
            }
        } 
    }
    xhr.send();
}