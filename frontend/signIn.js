window.onload = function(){
    const failText = document.getElementById("TemporaryFailText")
    failText.style.display = "none";
};

addEventListeners();

function addEventListeners(){
    const loginBtn = document.getElementById("loginSubmitBtn");
    loginBtn.addEventListener("click",logIn);
}


function logIn(event){
    event.preventDefault();
    const username = document.getElementById("signInUsername").value;
    const password = document.getElementById("signInPwd").value;
    const employee = {username,password};
    const payload = JSON.stringify(employee);
    performAjaxPostRequest("http://localhost:8080/revature-project1/login",payload,onSuccessfulLogin,onFailedLogin)
    
}

function onSuccessfulLogin(response){
    const employee = JSON.parse(response);
    sessionStorage.setItem("PlutoERSAuth",`${employee.id}:${(employee.manager) ? "MANAGER":"EMPLOYEE"}`);
    window.location.replace("./HomePage/EmployeeHomePage/EmployeeHomePage.html");
}

function onFailedLogin(){
    const failText = document.getElementById("TemporaryFailText");
    failText.style.display = "";
    setTimeout(()=>{
        failText.style.display = "none";
    },3000)
}

function performAjaxPostRequest(url, payload, successCallback, failureCallback){
    const xhr = new XMLHttpRequest();
    xhr.open("POST", url);
    xhr.onreadystatechange = function(){
        if(xhr.readyState==4 ){
            if(xhr.status>199 && xhr.status<300){
                successCallback(xhr.response); // this is going to be the response body of our http response  (JSON)
            } else {
                if(failureCallback){
                    failureCallback("Failed to update on the server")
                } else{
                    console.error("An error occurred while attempting to create a new record")
                }
            }
        }
    }
    xhr.send(payload);
}